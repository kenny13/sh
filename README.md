
Stream
-------

    rtmps://stream-ire-alfa.dropcam.com/nexus/62e26******************350874785de
    https://stream-ire-alfa.dropcam.com/nexus_aac/62e262d*******785de/playlist.m3u8

Media info
-----------

    Codec : H264-MPEG-4- AVC
    res: 1280x738
    Audio: MPEG ACC Audio Sterio

Store stream
----

    vlc -I dummy --run-time=60 https://stream-ire-alfa.dropcam.com/nexus_aac/62e262de7*********************0874785de/playlist.m3u8 --sout "#transcode{vcodec=h264,acodec=mpga,ab=128,channels=2,samplerate=44100}:file{mux=mp4,dst=out.mp4,access=file}" vlc://quit


    vlc -I dummy --run-time=60 https://stream-ire-alfa.dropcam.com/nexus_aac/62e262d**********e8350874785de/playlist.m3u8 --sout "#file{mux=mp4,dst=out.mp4,access=file}" vlc://quit

VLC params
------------

    https://wiki.videolan.org/VLC_command-line_help/

Run container
-------------

    docker run -it -v ~/work/sec-cam/out/:/home/vlc/ vlc https://stream-ire-alfa.dropcam.com/nexus_aac/62e262de78014fdf91de8350874785de/playlist.m3u8 vlc://quit


Build
-------

    docker build -t codondev/nestcam .
    docker build -t codondev/nestcam:0.11 .
    docker push codondev/nestcam:latest
    docker push codondev/nestcam:0.11

Run local
----------

    docker run -e "RUN_TIME=10" -e "AWS_ACCESS_KEY_ID=AK*****EA" -e "AWS_SECRET_ACCESS_KEY=E6******y" -e "STREAM_URL=https://stream-ire-alfa.dropcam.com/nexus_aac/62******de/playlist.m3u8" codondev/nestcam:0.3